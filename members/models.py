from django.db import models

class Utilisateur(models.Model):
    login = models.TextField()
    motDePasse = models.TextField()
    mail = models.TextField()
    
    
class StatutAmitie(models.Model):
    idUserAppelant = models.ForeignKey('Utilisateur', related_name = 'idUserAppelant')
    idUserAppele = models.ForeignKey('Utilisateur', related_name = 'idUserAppele')
    etatAmitie = models.BooleanField(default = False)
