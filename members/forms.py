from django import forms
from django.contrib.auth.forms import AuthenticationForm as DjangoAuthenticationForm  
from django.contrib.auth.models import User

class AuthenticationForm(DjangoAuthenticationForm):
    username = forms.CharField(label="Nom d'utilisateur", max_length=254, widget=forms.TextInput(attrs={"class": "form-control", 'placeholder':'Nom d\'utilisateur'}))
    password = forms.CharField(label="Mot de passe", widget=forms.PasswordInput(attrs={"class": "form-control", 'placeholder':'Mot de passe'}))
  
  
class RegistrationForm(forms.ModelForm):
    username = forms.CharField(label="Nom d'utilisateur", max_length=254, widget=forms.TextInput(attrs={"class": "form-control", 'placeholder':'Nom d\'utilisateur'}))
    email = forms.EmailField(label="Nom d'utilisateur", max_length=254, widget=forms.TextInput(attrs={"class": "form-control", 'placeholder':'Email'}))
    password = forms.CharField(label="Mot de passe", widget=forms.PasswordInput(attrs={"class": "form-control", 'placeholder':'Mot de passe'}))

     
    class Meta:
        model=User
        fields = ("username","email", "password")