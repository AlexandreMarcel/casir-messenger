from django.db import models

class Message(models.Model):
    message = models.TextField()
    date = models.DateTimeField(auto_now = True)
    etatLu = models.BooleanField(default = True)
    duree = models.PositiveSmallIntegerField()
    idExpediteur = models.ForeignKey(User, related_name = 'idExpediteur')
    idDestinataire = models.ForeignKey(User, related_name = 'idDestinataire')
    idPj = models.ForeignKey('PieceJointe')
